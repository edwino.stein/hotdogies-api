CREATE TABLE hotdogies.ingrediente
(
    id serial NOT NULL,
    nome character varying(50) NOT NULL,
    descricao character varying(255),
    dt_removido date,
    CONSTRAINT pk_ingredientes PRIMARY KEY (id)
);
