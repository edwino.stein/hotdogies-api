package org.edwino.garagem.hotdogiesapi.service;

import lombok.RequiredArgsConstructor;
import org.edwino.garagem.hotdogiesapi.model.Ingrediente;
import org.edwino.garagem.hotdogiesapi.repository.IngredienteRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class IngredienteService {

    private final IngredienteRepository repository;

    public List<Ingrediente> listar() {
        return this.repository.findAllByRemovidoEmIsNull(Sort.by(Sort.Direction.ASC, "nome"));
    }

    public Optional<Ingrediente> encontrarUmPorId(Long id) {
        return this.repository.findOneByIdAndRemovidoEmIsNull(id);
    }
}
