package org.edwino.garagem.hotdogiesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotdogiesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotdogiesApiApplication.class, args);
	}

}
