package org.edwino.garagem.hotdogiesapi.repository;

import org.edwino.garagem.hotdogiesapi.model.Ingrediente;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IngredienteRepository extends JpaRepository<Ingrediente, Long> {
    List<Ingrediente> findAllByRemovidoEmIsNull(Sort sort);
    Optional<Ingrediente> findOneByIdAndRemovidoEmIsNull(Long id);
    Optional<Ingrediente> findOneByNomeAndRemovidoEmIsNull(String nome);
}
