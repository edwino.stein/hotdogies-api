-- Script para inicializar o banco de testes depois que o migration for executado

-- Inicializa a tabela ingredientes para testes
INSERT INTO hotdogies.ingrediente(nome, descricao, dt_removido) VALUES
    ('Salsicha', 'Ingrediente base do recheio', null),
    ('Pão', 'Ingrediente básico', null),
    ('Batata Palha', 'Ingrediente para cobertura', null),
    ('Milho', 'Ingrediente para o recheio', '2020-08-25');