package org.edwino.garagem.hotdogiesapi.dataset;

import org.edwino.garagem.hotdogiesapi.model.Ingrediente;

import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class IngredienteDataset {

    private Ingrediente[] ingredientes = {
        new Ingrediente().setId(1l).setNome("Salsicha").setDescricao("Ingrediente base do recheio").setRemovidoEm(null),
        new Ingrediente().setId(2l).setNome("Pão").setDescricao("Ingrediente básico").setRemovidoEm(null),
        new Ingrediente().setId(3l).setNome("Batata Palha").setDescricao("Ingrediente para cobertura").setRemovidoEm(null),
        new Ingrediente().setId(4l).setNome("Milho").setDescricao("Ingrediente para o recheio").setRemovidoEm(new Date())
    };

    public List<Ingrediente> getList(){
        return List.of(this.ingredientes);
    }

    public List<Ingrediente> filter(Predicate<Ingrediente> f){
        return this.getList().stream().filter(f).collect(Collectors.toList());
    }
}
