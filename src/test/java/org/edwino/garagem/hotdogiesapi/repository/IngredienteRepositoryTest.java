package org.edwino.garagem.hotdogiesapi.repository;

import org.edwino.garagem.hotdogiesapi.model.Ingrediente;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//Se precisar inicializar com dados especificos
//@Sql({"/db/inicializar_ingredientes.sql"})
public class IngredienteRepositoryTest {

    @Autowired
    private IngredienteRepository repository;

    @Test
    void verificarSeORepostitoryFoiInstanciado(){
        assertThat(this.repository).isNotNull();
    }

    @Test
    void verificaABuscaPorTodos(){

        // Busca por todos itens sem nenhum filtro
        List<Ingrediente> resultado = this.repository.findAll(Sort.by(Sort.Direction.ASC, "nome"));

        // Deve retornar todos os itens (4)
        assertThat(resultado.size()).isEqualTo(4);
    }

    @Test
    void verificaABuscaPorTodosNaoRemovidos(){

        // Busca por todos itens que não foram removidos
        List<Ingrediente> resultado = this.repository.findAllByRemovidoEmIsNull(Sort.by(Sort.Direction.ASC, "nome"));

        // O tamanho da lista deve ser igual a quantiadade de itens não removidos (3)
        assertThat(resultado.size()).isEqualTo(3);

        // Todos os itens não podem ter sido removidos
        resultado.forEach(i -> assertThat(i.getRemovidoEm()).isNull());
    }

    @Test
    void verificaABuscaPorIdEQueNaoFoiRemovido(){

        // Busca pelo item com ID 1 e que não deve ter sido removido
        Optional<Ingrediente> resultado = this.repository.findOneByIdAndRemovidoEmIsNull(1l);

        // Tem que retornar um item
        assertThat(resultado.isPresent()).isTrue();

        // O item tem que ser igual ao esperado
        assertThat(resultado.get().getId()).isEqualTo(1l);
    }

    @Test
    void verificaABuscaPorIdEQueFoiRemovido(){

        // Busca pelo item com ID 4 e que deve ter sido removido
        Optional<Ingrediente> resultado = this.repository.findOneByIdAndRemovidoEmIsNull(4l);

        // Não deve retornar o item
        assertThat(resultado.isPresent()).isFalse();
    }

    @Test
    void verificaABuscaPorNomeEQueNaoFoiRemovido(){

        // Busca pelo item com nome "Salsicha" e que não deve ter sido removido
        Optional<Ingrediente> resultado = this.repository.findOneByNomeAndRemovidoEmIsNull("Salsicha");

        // Tem que retornar um item
        assertThat(resultado.isPresent()).isTrue();

        // O itens não podem ter sido removido
        assertThat(resultado.get().getRemovidoEm()).isNull();

        // O item tem que ser igual ao esperado
        assertThat(resultado.get().getNome()).isEqualTo("Salsicha");
    }

    @Test
    void verificaABuscaPorNomeEQueFoiRemovido(){

        // Busca pelo item com nome "Milho" e que deve ter sido removido
        Optional<Ingrediente> resultado = this.repository.findOneByNomeAndRemovidoEmIsNull("Milho");

        // Não deve retornar o item
        assertThat(resultado.isPresent()).isFalse();
    }
}
