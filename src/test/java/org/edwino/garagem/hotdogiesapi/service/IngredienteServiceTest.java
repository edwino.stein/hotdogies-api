package org.edwino.garagem.hotdogiesapi.service;

import org.edwino.garagem.hotdogiesapi.dataset.IngredienteDataset;
import org.edwino.garagem.hotdogiesapi.model.Ingrediente;
import org.edwino.garagem.hotdogiesapi.repository.IngredienteRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredienteServiceTest {

    @InjectMocks
    private IngredienteDataset dataset;

    @Mock
    private IngredienteRepository repository;

    @InjectMocks
    private IngredienteService service;

    @Test
    void verificarAListagem(){

        // Pega a lista no dataset de teste
        List<Ingrediente> ingredientes = this.dataset.filter(i -> i.getRemovidoEm() == null);
        ingredientes.sort((a, b) -> a.getNome().compareTo(b.getNome()));

        // Mockeia o método do repository
        when(this.repository.findAllByRemovidoEmIsNull(
            eq(Sort.by(Sort.Direction.ASC, "nome")))
        ).thenReturn(ingredientes);

        // Chama a regra de negócio
        List<Ingrediente> resultado = this.service.listar();

        // Verifica se o tamanho da lista é igual a quantidade de itens não removidos
        assertThat(resultado.size()).isEqualTo(3);

        // Nenhum item do resultado deve ter sido removido
        resultado.forEach(i -> assertThat(i.getRemovidoEm()).isNull());

        // Pega o primerio e último para verificar a ordenação
        Ingrediente primeiro = resultado.get(0);
        Ingrediente ultimo = resultado.get(resultado.size() - 1);

        // O primerio tem que ser o id 3 (Batata Palha)
        assertThat(primeiro.getId()).isEqualTo(3);

        // O último tem que ser o id 1 (Salsicha)
        assertThat(ultimo.getId()).isEqualTo(1);
    }

    @Test
    void verificarABuscaDeUmPorIdValido(){

        // Pega a instância do ID 1
        Ingrediente ingrediente = this.dataset.filter(i -> i.getId() == 1l).get(0);

        // Mockeia o método do repository
        when(this.repository.findOneByIdAndRemovidoEmIsNull(anyLong())).thenReturn(Optional.of(ingrediente));

        // Chama a regra de negócio
        Optional<Ingrediente> resultado = this.service.encontrarUmPorId(1l);

        // O item retornado tem que ser válido
        assertThat(resultado.isPresent()).isTrue();

        // Não pode ter sido removido
        assertThat(resultado.get().getRemovidoEm()).isNull();

        // Tem que ser igual ao esperado
        assertThat(resultado.get()).isSameAs(ingrediente);
    }

    @Test
    void verificarABuscaDeUmPorIdInvalido(){

        // Mockeia o método do repository
        when(this.repository.findOneByIdAndRemovidoEmIsNull(anyLong())).thenReturn(Optional.empty());

        // Chama a regra de negócio
        Optional<Ingrediente> resultado = this.service.encontrarUmPorId(0l);

        // O item retornado tem que ser inválido
        assertThat(resultado.isPresent()).isFalse();
    }
}
